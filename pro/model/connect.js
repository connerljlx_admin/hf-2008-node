const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/qfadmin',{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're conected!
  console.log('mongodb上线了')
});
