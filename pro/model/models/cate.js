const mongoose = require('mongoose');
// 创建schema 表的结构
const cateSchema = new mongoose.Schema({
  cateName: { // 分类名称
    type: String,
    required: true
  },
  cateIcon: String, // 分类图标
  cateDesc: String, // 分类描述
  pid:{
    type: String,
    default: '0' // 0 代表顶层分类
  }
})

const cateModel = mongoose.model('qf_cates', cateSchema);

module.exports = cateModel;
/* 
顶层分类
电脑  0   （1）
  笔记本 台式机 组装机
  pid 1
手机 0    3
    翻盖手机 pid 3
家用电器
*/