const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const homeRouter = require('./routes/home');
const cateRouter = require('./routes/cate');
const itemRouter = require('./routes/item');
const uploadRouter = require('./routes/upload')
require('./model/connect');

const app = express();

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use('/static', express.static(path.join(__dirname,'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')

app.use(homeRouter);
app.use(cateRouter);
app.use(itemRouter);
app.use(uploadRouter);
app.listen(3000, ()=>{
  console.log('start at port 3000')
})