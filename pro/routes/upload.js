const router = require('express').Router();
const multer = require('multer');
const path = require('path')
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/uploads')
  },
  filename: function (req, file, cb) {
    // 自定义文件名的
    /* console.log(file);
    console.log(path.extname(file.originalname)) */
    // 后缀名
    const extname = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + Date.now()+extname);
  }
})
const upload = multer({ storage: storage })
// 创建上传图片的路由


router.post('/upload', upload.single('img'), (req,res)=>{
  console.log(req.file)
  const filePath = '/static/uploads/'+req.file.filename;
  res.send({
    code:0,
    msg:'上传成功',
    data: {
      filePath
    }
  })
});

module.exports = router;