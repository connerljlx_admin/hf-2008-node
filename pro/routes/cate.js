const cateModel = require('../model/models/cate')
const router = require('express').Router();
// 分类列表
router.get('/cateList', async (req,res)=>{
  /* 
  current 当前页
  pageSize 一页多少条
  */
  let { current=1,pageSize=2 } = req.query;
  current = parseInt(current);
  pageSize = parseInt(pageSize);
  const result = await cateModel.find().limit(pageSize).skip((current-1)*pageSize).sort({
    _id: -1
  });
  // 总条数
  const counts = await cateModel.count();
  res.render('cateList', {
    cates: result,
    counts,
    current
  })
})
router.get('/cateAdd', (req,res)=>{
  /* 
  取所有分类 
  */
  cateModel.find().then(ret=>{
    res.render('cateAdd',{
      cates: ret
    })
  }).catch(err=>{
    res.render('cateAdd',{
      cates: []
    })
  })
  
})

router.post('/cateAdd', (req, res)=>{
  
  cateModel.insertMany(req.body).then(ret=>{
    res.send({
      code: 0, // 接口有误问题 状态码 ajax http
      msg: '增加成功' 
    })
  }).catch(err=>{
    res.send({
      code: -1, // 接口有误问题 状态码 ajax http
      msg: '增加失败' 
    })
  })
 
})
router.post('/delCate', (req,res)=>{
  const { _id } = req.body;
  cateModel.remove({
    _id
  }).then(ret=>{
    res.send({
      code: 0,
      msg: '删除成功'
    })
  }).catch(err=>{
    res.send({
      code: -1,
      msg: '删除失败'
    })
  })
 
})
router.get('/cateEdit', async (req,res)=>{
  // 获取所有分类 作为 父级分类
  // 通过id获取这条数据初始值
  const {id} = req.query;
  const cates = await cateModel.find();
  const cate = await cateModel.findOne({
    _id: id
  });
  console.log(cate)
  res.render('cateEdit', {
    cates,
    cate
  })
})

router.post('/cateEdit', (req,res)=>{
  cateModel.update({_id: req.body._id},req.body).then(ret=>{
    res.send({
      code:0,
      msg: '修改成功'
    })
  }).catch(err=>{
    res.send({
      code:-1,
      msg: '修改失败'
    })
  })
  
})

module.exports = router;