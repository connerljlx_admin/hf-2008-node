const fs = require('fs');
// 读取文件 readFile
/* fs.readFile('./a.txt',{
  encoding: 'utf8'
}, (err, data)=>{
  if(err){
    // 报错了
    console.log('读取失败');
    return;
  }
  // console.log(data.toString());
  console.log(data)
}) */
// 同步语法
let data = "";
try{
  data = fs.readFileSync('./a.tx');
}catch(err){
  console.log(err)
  console.log('读取失败')
}

/* const error1 = new Error('自定义错误');
throw(error1) */
/* 
回调函数的设计规范：
  回调的错误优先（优先将可能出现的错误作为回调函数的第一个参数传入）
*/


/* try{
  console.log(a)
}catch(err){
  console.log('走了catch')
} */

// console.log(222)