// 启动一个服务器
const express = require('express');
const app = express();

const middleware1 = (req,res,next)=>{
  console.log('1');
  next();
  console.log('1-1')
}
const middleware2 = (req,res,next)=>{
  console.log('2');
  next();
  console.log('2-2')
}
const middleware3 = (req,res,next)=>{
  console.log('3');
  next();
  console.log('3-3')
}
app.use(middleware1);
app.use(middleware2);
app.use(middleware3);

app.get('/a', (req,res)=>{
  res.send('你好啊-----'); // 参数可以是字符串 或者 对象
})

app.post('/b', (req,res)=>{
  console.log(req.body);
  res.send('我是一个post请求')
});


app.listen(3000, ()=>{
  console.log('start at port 3000')
})