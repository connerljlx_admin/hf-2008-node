// 启动一个服务器
const express = require('express');
const bodyParser = require('body-parser');

const app = express();
// 解析 x-www-form-urlencoded 格式 "key=v&key=v" {}
app.use(bodyParser.urlencoded({
  extended: false
}));
// 解析 application/json格式
app.use(bodyParser.json());

app.get('/a', (req,res)=>{
  res.send('你好啊-----'); // 参数可以是字符串 或者 对象
})

app.post('/b', (req,res)=>{
  console.log(req.body);
  res.send('我是一个post请求')
});


app.listen(3000, ()=>{
  console.log('start at port 3000')
})