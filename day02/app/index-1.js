// 启动一个服务器
const express = require('express');
const app = express();

/* 
中间件其实就是一个函数
code review
*/
const middleware1 = (req,res,next)=>{
  /* 
  next前面 拦截 请求
  */
  req.aaa = 100;
  next();
  /* 
  拦截响应
  */
}
/* 
使用中间件
app.use([url,]中间件)
url指定 中间件拦截哪个路由
如果省略 那么就是全局中间件（会拦截所有的请求）
*/
// app.use('/a', middleware1);
const globalMiddleware = (req,res,next)=>{
  console.log('全局中间件执行力');
  next();
}
app.use(globalMiddleware);

app.get('/a', (req,res)=>{
  // 解析get请求的参数
  console.log(req.aaa);
  res.send('你好啊-----'); // 参数可以是字符串 或者 对象
})

app.post('/b', (req,res)=>{
  console.log(req.body);
  res.send('我是一个post请求')
});


app.listen(3000, ()=>{
  console.log('start at port 3000')
})