// 启动一个服务器
const express = require('express');
const bodyParser = require('body-parser');
const user = {
  username: '小明',
  pwd: 123456
}
const app = express();
// 解析 x-www-form-urlencoded 格式 "key=v&key=v" {}
app.use(bodyParser.urlencoded({
  extended: false
}));
// 解析 application/json格式
app.use(bodyParser.json());


app.post('/login', (req,res)=>{
  /* 
  username
  pwd
  */
  const { username, pwd } = req.body;
  if(username === user.username){
    // 判断密码对不对
    if(parseInt(pwd) === parseInt(user.pwd)){
      res.send('登录成功')
    }else{
      res.send('密码错误')
    }
  }else{
    res.send('没有当前用户')
  }
});


app.listen(3000, ()=>{
  console.log('start at port 3000')
})