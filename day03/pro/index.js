const express = require('express');
const bodyParser = require('body-parser');
const homeRouter = require('./routes/home');
const newsRouter = require('./routes/news');
const uploadRouter = require('./routes/upload')
const path = require('path');
const app = express(); 

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
// 定义静态资源中间件
app.use('/static', express.static(path.join(__dirname, 'public')))

// 设置模板（views）的目录
app.set('views', path.join(__dirname,'views'))
// 设置使用哪个模板引擎
app.set('view engine', 'ejs')


app.use(homeRouter);
app.use(newsRouter);
app.use(uploadRouter);
app.listen(9527, ()=>{
  console.log('start at port 9527')
})