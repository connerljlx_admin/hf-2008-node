const express = require('express');
const bodyParser = require('body-parser');
/* const router = express.Router();*/
const homeRouter = require('./routes/home')
const app = express(); 
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* router.get('/a', (req,res)=>{
  res.send('a页面')
})
router.post('/b', (req,res)=>{
  res.send('post请求')
})

// 使用中间件
app.use(router); */
app.use(homeRouter);
app.listen(9527, ()=>{
  console.log('start at port 9527')
})