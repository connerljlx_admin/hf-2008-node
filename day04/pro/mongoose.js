const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test',{
  useNewUrlParser: true,
  useUnifiedTopology: true
});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're conected!
  console.log('mongodb上线了')
});

/*
schema
理解为 集合（collection) 结构，
定义一个集合的字段以及字段类型
（操作集合如增加一条数据，都会经过schema验证，验证通过允许操作，否则报错）
*/ 

// 学生表结构  schema
const stuSchema = new mongoose.Schema({
  a: String,
  b: {
    type: Number,
    required: true
  },
  c: {
    type: String,
    default: '我是默认值'
  }
});
// 创建表的实例 model
const stuModel = mongoose.model('qfStu', stuSchema);
stuModel.insertMany([
  {
    a:111,
    b: 222
  }
]).then(res=>{
  console.log('成功了')
}).catch(err=>{
  console.log('失败了')
}) 
/* 
stuModel.insertMany([
  {
    a:111,
    b: 222
  }
]).then(res=>{
  console.log('成功了')
}).catch(err=>{
  console.log('失败了')
}) 
*/
/* stuModel.find().then(res=>{
  console.log(res)
}).catch(err=>{
  console.log('查询失败')
}) */